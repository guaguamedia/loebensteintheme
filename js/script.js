$(document).ready(function() {
 
  $("#main-slide").owlCarousel({
 
      navigation : false, // Show next and prev buttons
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem:true,
      pagination: false,
      autoPlay: true
 
  });
 
});