//==== gulp init =====
//npm install gulp
//npm i -D gulp-sass gulp-minify-css gulp-watch browser-sync

'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');
var reload = browserSync.reload;
var minifyCSS = require('gulp-minify-css');
var watch = require('gulp-watch');

gulp.task('css', function () {
   gulp.src('./css/styles.scss')
   .pipe(sass())
   .pipe(minifyCSS())
   .pipe(gulp.dest('./css'));
});

gulp.task('serve',['css'], function(){
	browserSync.init(["./css/**/*.scss", "./js/**/*.js", "./*.html"],{
		server:{
			baseDir: './'
		}
	}
	);
});

gulp.task('watch', ['css', 'serve'], function () {
   gulp.watch(['./css/**/*.scss'], ['css']);
});
